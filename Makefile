# Binaries
WEBPACK = node_modules/.bin/webpack
MOCHA = node_modules/.bin/mocha

# Files
TEST = $(wildcard test/*.js test/**/*.js test/*.jsx test/**/*.jsx)

test:
	@$(MOCHA) --compilers js:babel-core/register --require babel-polyfill $(TEST)

testd:
	@$(MOCHA) debug --compilers js:babel-core/register --require babel-polyfill $(TEST)

build: main.js
	@$(WEBPACK)

clean:
	@rm bundle.js


.PHONY: test clean
