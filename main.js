import React     from 'react';
import ReactDOM  from 'react-dom';
import CGOLStore from './src/stores/cgol';
import { Board } from './src/components/board';

const columns = Math.ceil(window.innerWidth / 10);
const rows    = Math.ceil(window.innerHeight / 10);

let board = [];

for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
  let row = [];

  for (let column = 0; column < columns; column++) {
    row.push(Math.ceil(Math.random() * 100) > 85 ? 1 : 0);
  }

  board.push(row);
}

CGOLStore.dispatch({ type: 'SET_BOARD', board: board });

const revive = (row, column) => {
  CGOLStore.dispatch({ type: 'REVIVE', row: row, column: column });
};

CGOLStore.subscribe(() => {
  ReactDOM.render(
    <Board board={ CGOLStore.getState().board } revive={ revive } />,
    document.getElementById('root')
  );
});

const interval = setInterval(() => {
  CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });
}, 1 * 1000);

