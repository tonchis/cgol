The goal of this project is to [deliberately practice](http://expertenough.com/1423/deliberate-practice) modern web JS tooling such as:

* ES6
* ReactJS
* Redux (or some form of Flux)
* Docker and Kubernetes
* GitLab CI/CD features