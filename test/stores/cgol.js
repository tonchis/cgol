import assert    from 'assert';
import CGOLStore from '../../src/stores/cgol';

describe('Conway Game Of Life Store', () => {
  describe('Initialize store', () => {
    it('starts with an empty board', () => {
      assert.deepEqual(CGOLStore.getState().board, []);
    });

    it('ignores unknown actions', () => {
      assert.deepEqual(CGOLStore.getState().board, []);

      CGOLStore.dispatch({ type: '¯\_(ツ)_/¯' });

      assert.deepEqual(CGOLStore.getState().board, []);
    });
  });

  // https://en.wikipedia.org/wiki/Conway's_Game_of_Life#Rules
  describe('Rules (3x3 board)', () => {
    describe('Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.', () => {
      it('center cell dies alone', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [0, 0, 0],
            [0, 1, 0],
            [0, 0, 0],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
          ],
          'center cell didn\'t die'
        );
      });

      it('center and top right cells die together', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [0, 0, 1],
            [0, 1, 0],
            [0, 0, 0],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
          ],
          'center and top right cells didn\'t die'
        );
      });

      it('corner cells die isolated', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [1, 0, 1],
            [0, 0, 0],
            [1, 0, 1],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
          ],
          'some corner cells didn\'t die'
        );
      });
    });

    describe('Any live cell with two or three live neighbours lives on to the next generation.', () =>  {
      it('center cell with two neighbours lives', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [0, 0, 1],
            [0, 1, 0],
            [1, 0, 0],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [0, 0, 0],
            [0, 1, 0],
            [0, 0, 0],
          ],
          'center cell died'
        );
      });

      it('center cell with three neighbours lives', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [1, 0, 1],
            [0, 1, 0],
            [1, 0, 0],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [0, 1, 0],
            [1, 1, 0],
            [0, 0, 0],
          ],
          'center cell died'
        );
      });
    });

    describe('Any live cell with more than three live neighbours dies, as if by overpopulation.', () => {
      it('center cell with corner neighbours die', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [1, 0, 1],
            [0, 1, 0],
            [1, 0, 1],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [0, 1, 0],
            [1, 0, 1],
            [0, 1, 0],
          ],
          'dead cells didn\'t blink back to life'
        );
      });

      it('full board, only corner cells survive', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [1, 1, 1],
            [1, 1, 1],
            [1, 1, 1],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [1, 0, 1],
            [0, 0, 0],
            [1, 0, 1],
          ],
          'some cells other than the corners survived'
        );
      });
    });

    describe('Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.', () => {
      it('first row with living cells revive center cell', () => {
        CGOLStore.dispatch({
          type: 'SET_BOARD',
          board: [
            [1, 1, 1],
            [0, 0, 0],
            [0, 0, 0],
          ]
        });

        CGOLStore.dispatch({ type: 'ADVANCE_GENERATION' });

        assert.deepEqual(
          CGOLStore.getState().board,
          [
            [0, 1, 0],
            [0, 1, 0],
            [0, 0, 0],
          ],
          'center cell is still dead'
        );
      });
    });
  });
});
