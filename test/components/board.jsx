import React                from 'react';
import assert               from 'assert';
import jsdom                from 'jsdom';
import { shallow, mount }   from 'enzyme';
import { Board, Row, Cell } from '../../src/components/board';

const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.document = doc;
global.window = doc.defaultView;

describe('<Board />', () => {
  it('needs a board and revive functions', () => {
    assert.doesNotThrow(() => {
      shallow(<Board board={ [[]] } revive={ function(){} } />);
    });
  });

  it('renders Rows', () => {
    const board = [
      [0, 0, 1],
      [0, 1, 0],
      [0, 0, 0],
    ]
    const revive = (board, row, column) => {};

    const wrapper = shallow(<Board board={ board } revive={ revive }/>);

    assert.equal(3, wrapper.find(Row).length);
  });
});

describe('<Row />', () => {
  it('needs an array and a revive function', () => {
    assert.doesNotThrow(() => {
      shallow(<Row row={ [] } revive={ function(){} } />);
    });
  });

  it('renders Cells', () => {
    const row    = [0, 1, 1];
    const revive = (board, row, column) => {};

    const wrapper = shallow(<Row row={ row } revive={ revive }/>);
    const cells = wrapper.find(Cell);

    assert.equal(3, cells.length);
    assert.equal(2, cells.findWhere((cell) => { return  cell.prop('alive') }).length);
    assert.equal(1, cells.findWhere((cell) => { return !cell.prop('alive') }).length);
  });
});
