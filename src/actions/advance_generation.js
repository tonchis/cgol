import clone from 'clone';

const advanceGeneration = (state) => {
  const nextGenerationBoard = clone(state.board);

  const paddedBoard = addPadding(clone(state.board));
  // The `paddedBoard` is the same as the `board` but with an extra row/column of zeroes on the edges.
  //
  // Example board:
  //   [1, 0, 1]
  //   [0, 0, 0]
  //   [1, 0, 1]
  //
  // Example paddedBoard:
  //   [0, 0, 0, 0, 0]
  //   [0, 1, 0, 1, 0]
  //   [0, 0, 0, 0, 0]
  //   [0, 1, 0, 1, 0]
  //   [0, 0, 0, 0, 0]
  //
  // We do this so we don't have to worry about the indexes when `countLiveNeighbours`. This way all the cells have exactly 8 neighbours.

  for (let [rowIndex, row] of state.board.entries()) {
    for (let [columnIndex, cell] of row.entries()) {
      const liveNeighbours = countLiveNeighbours(paddedBoard, rowIndex, columnIndex);

      if (cell === 1 && liveNeighbours < 2) {
        nextGenerationBoard[rowIndex][columnIndex] = 0;
      } else if (cell === 1 && liveNeighbours > 3) {
        nextGenerationBoard[rowIndex][columnIndex] = 0;
      } else if (cell === 0 && liveNeighbours === 3) {
        nextGenerationBoard[rowIndex][columnIndex] = 1;
      }
    }
  }

  return { board: nextGenerationBoard, internalStructure: state.internalStructure };
}

const countLiveNeighbours = (paddedBoard, rowIndex, columnIndex) => {
  const paddedRowIndex = rowIndex + 1;
  const paddedColumnIndex = columnIndex + 1;

  let neighbours = [
    paddedBoard[paddedRowIndex - 1][paddedColumnIndex - 1], // top left
    paddedBoard[paddedRowIndex - 1][paddedColumnIndex],     // top center
    paddedBoard[paddedRowIndex - 1][paddedColumnIndex + 1], // top right

    paddedBoard[paddedRowIndex][paddedColumnIndex - 1],     // center left
    paddedBoard[paddedRowIndex][paddedColumnIndex + 1],     // center right

    paddedBoard[paddedRowIndex + 1][paddedColumnIndex - 1], // bottom left
    paddedBoard[paddedRowIndex + 1][paddedColumnIndex],     // bottom center
    paddedBoard[paddedRowIndex + 1][paddedColumnIndex + 1], // bottom right
  ];

  return neighbours.reduce((accum, neighbour) => {
    return accum + neighbour;
  });
}

const addPadding = (board) => {
  for (let row of board) {
    row.unshift(0);
    row.push(0);
  }

  let paddedBoard = [new Array(board.length + 2).fill(0)];

  for (let row of board) {
    paddedBoard.push(row);
  }

  paddedBoard.push(new Array(board.length + 2).fill(0));

  return paddedBoard;
}

export { advanceGeneration };
