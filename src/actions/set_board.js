const setBoard = (board) => {
  return {
    board: board,
    internalStructure: transformBoardIntoInternalStructure(board)
  };
};

const transformBoardIntoInternalStructure = (board) => {
  return board;
};

const transformInternalStructureIntoBoard = (internalStructure) => {
  return internalStructure;
};

export { setBoard };
