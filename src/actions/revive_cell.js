import clone from 'clone';

const reviveCell = (state, row, column) => {
  const newBoard = clone(state.board);

  newBoard[row][column] = 1;

  return { board: newBoard, internalStructure: state.internalStructure };
}

export { reviveCell };
