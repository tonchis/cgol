import React    from 'react';
import ReactDOM from 'react-dom';

class Board extends React.Component {
  render() {
    const board = this.props.board.map((row, index) => {
      return <Row row={ row } key={ index } revive={ this.props.revive.bind(this, index) }/>
    });

    return (
      <table style={ this.style() }>
        <thead></thead>
        <tbody>{ board }</tbody>
      </table>
    );
  }

  style() {
    return {
      width: '100%',
      height: '100%',
      borderCollapse: 'collapse'
    };
  }
}

class Row extends React.Component {
  render() {
    const cells = this.props.row.map((cell, index) => {
      return <Cell alive={ cell === 1 } key={ index } revive={ this.props.revive.bind(this, index) }/>;
    });

    return <tr>{ cells }</tr>;
  }
}

class Cell extends React.Component {
  render() {
    return <td onClick={ this.props.revive } style={ this.style() }></td>
  }

  style() {
    return {
      padding: '0 0',
      width: '10px',
      height: '10px',
      backgroundColor: ( this.props.alive ? 'black' : 'white')
    };
  }
}

export { Board, Row, Cell };
