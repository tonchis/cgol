import { createStore }       from 'redux';
import { advanceGeneration } from '../actions/advance_generation';
import { reviveCell }        from '../actions/revive_cell';
import { setBoard }          from '../actions/set_board';

const reducer = (state, action) => {
  if (typeof state === 'undefined') {
    return { board: [], internalStructure: [] };
  }

  switch (action.type) {
    case 'ADVANCE_GENERATION':
      return advanceGeneration(state);
    case 'SET_BOARD':
      return setBoard(action.board);
    case 'REVIVE':
      return reviveCell(state, action.row, action.column);
    default:
      return state;
  }
};

export default createStore(reducer);
